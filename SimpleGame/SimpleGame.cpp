/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

/*
[ 속도, 가속도, 힘의 관계 ]

갱신될 위치: 이전 위치 + 속도(단위시간당 위치의 변화량) * 시간	(=> 속도가 있어야 위치가 변화된다!!)
갱신될 속도: 이전 속도 + 가속도(단위시간당 속도의 변화량) * 시간

가속도 = 힘 / 질량 (=> 힘이 변하면 속도가 변하기때문에 힘이 가속도와 연관되어있다)
				   (힘의 변화가 없다 = 속도의 변화가 없다 = 가속도가 없다)

힘 = wasd 기반 힘의 방향 결정
*/

/*
[ 마찰력 ]
	운동 방향의 반대로 항시 작용하는 힘.
	마찰력은 수직 항력에 비례해서 계산된다.
	마찰력 = 마찰계수 * mg (마찰 계수는 오브젝트마다 있어야 함)
	항시 작용해야하므로 update()에서 구현

	{ 마찰력 -> 가속도 } == 속도변화이므로 { } 부분을 구현하면 됨.
	반드시 속도가 있을 때만 작용하고 속도가 없다면 마찰력도 작용하지 않도록 예외처리를 해야함.
*/

// TIP. 화면에 무언가 나오지않을 때는 draw하는 함수에 break를 걸어두고 디버깅을 해보면 된다!!

#include "stdafx.h"
#include <iostream>
#include <Windows.h>	// 선언 순서 명시!
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "ScnMgr.h"
#include "Global.h"

ScnMgr* g_ScnMgr = NULL;

DWORD g_PrevTime = 0;

bool g_KeyW = FALSE;
bool g_KeyA = FALSE;
bool g_KeyS = FALSE;
bool g_KeyD = FALSE;
bool g_KeySP = FALSE;

bool g_KeyUp = FALSE;
bool g_KeyDown = FALSE;
bool g_KeyLeft = FALSE;
bool g_KeyRight = FALSE;

int g_Shoot = SHOOT_NONE;

void RenderScene(void)
{
	// calc elapsed time
	if (g_PrevTime == 0)	// g_PrevTime은 0이고 currTime은 시작부터 시간을 재고 있기때문에 처음 elapsedTime을 구할 때 차이가 너무 많아 나버릴 수 있다.
	{
		//g_PrevTime = timeGetTime();
		g_PrevTime = GetTickCount();
		return;
	}
	DWORD currTime = GetTickCount();		// current time in millisec
	DWORD elapsedTime = currTime - g_PrevTime;
	g_PrevTime = currTime;
	float eTime = (float)elapsedTime / 1000.f;		// ms to s

	//std::cout << "elapsed time: " << eTime << std::endl;		// 시간 확인 출력


	// 힘 적용
	float forceX = 0.f;
	float forceY = 0.f;
	float forceZ = 0.f;
	float amount = 1.4f;
	float zAmount = 250.f;
	if (g_KeyW)
	{
		forceY += amount;
	}
	if (g_KeyS)
	{
		forceY -= amount;
	}
	if (g_KeyA)
	{
		forceX -= amount;
	}
	if (g_KeyD)
	{
		forceX += amount;
	}
	if (g_KeySP)
	{
		forceZ += zAmount;
	}

	// Normalize(&fx, &fy)
	g_ScnMgr->ApplyForce(forceX, forceY, forceZ, eTime);
	g_ScnMgr->Update(eTime);
	g_ScnMgr->RenderScene();
	g_ScnMgr->Shoot(g_Shoot);
	g_ScnMgr->DoCollisionTest();
	g_ScnMgr->GarbageCollector();


	glutSwapBuffers();
}

void Idle(void)			// 계속 실행된다.
{
	RenderScene();
}

// 이 밑에 있는 함수들로 마우스나 키보드로 기능을 만들 때 사용
void MouseInput(int button, int state, int x, int y)
{
	RenderScene();
}

void KeyDownInput(unsigned char key, int x, int y)
{
	int scene = -1;
	g_ScnMgr->GetScene(&scene);

	if (scene == GAMEPLAY_SCENE)
	{
		if (key == 'w' || key == 'W') {
			g_KeyW = TRUE;
		}
		else if (key == 'a' || key == 'A') {
			g_KeyA = TRUE;
		}
		else if (key == 's' || key == 'S') {
			g_KeyS = TRUE;
		}
		else if (key == 'd' || key == 'D') {
			g_KeyD = TRUE;
		}
		else if (key = ' ')
		{
			g_KeySP = TRUE;
		}
	}
}

void KeyUpInput(unsigned char key, int x, int y)
{
	int scene = -1;
	g_ScnMgr->GetScene(&scene);

	switch (scene)
	{
	case TITLE_SCENE:
		if (key == VK_RETURN)
		{
			g_ScnMgr->SetScene(GAMEPLAY_SCENE);
		}
		break;
	case GAMEPLAY_SCENE:
		if (key == 'w' || key == 'W') {
			g_KeyW = FALSE;
		}
		else if (key == 'a' || key == 'A') {
			g_KeyA = FALSE;
		}
		else if (key == 's' || key == 'S') {
			g_KeyS = FALSE;
		}
		else if (key == 'd' || key == 'D') {
			g_KeyD = FALSE;
		}
		else if (key = ' ')
		{
			g_KeySP = FALSE;
		}
		break;
	case END_SCENE:
		if (key)
		{
			exit(1);
		}
		break;
	default:
		break;
	}

}

void SpecialKeyDownInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		g_Shoot = SHOOT_UP;
		break;
	case GLUT_KEY_DOWN:
		g_Shoot = SHOOT_DOWN;
		break;
	case GLUT_KEY_LEFT:
		g_Shoot = SHOOT_LEFT;
		break;
	case GLUT_KEY_RIGHT:
		g_Shoot = SHOOT_RIGHT;
		break;
	}
}

void SpecialKeyUpInput(int key, int x, int y)
{
	g_Shoot = SHOOT_NONE;
}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(700, 500);
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);	// 키를 누르고 있으면 계속 누르고 있다고 알려주는 기본 설정을 해제
	glutCreateWindow("Game Software Engineering KPU");		// 이 순간 설정한 윈도우 창이 하나 & 콘솔 창 하나가 나옴

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	// callback 함수 등록(function 포인터)
	// 해당 이벤트가 발생했을 때 (함수)를 호출해라! 라는 것
	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);		// 아무 것도 하지않는 상태에서 호출
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);
	
	glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

	// Initialize Scene Manager
	g_ScnMgr = new ScnMgr();
	
	glutMainLoop();		// 렌더링과 키입력 등을 계속 루프를 돌고 프로그램 종료 시까지 유지, 렌더씬을 매번 호출

	delete g_ScnMgr;

	return 0;
}