#include "stdafx.h"
#include "Object.h"
#include "Global.h"
#include <math.h>
#include <float.h>

Object::Object()
{
	Age = 0.f;
	MaxAge = FLT_MAX;

	CoolTime = 0.15f;
	CurrCoolTime = CoolTime;
}


Object::~Object()
{
}

void Object::Update(float eTime)
{
	// win api를 사용 timegetrime(): 프로그램이 시작되고 누적된 시간값을 밀리세컨드로 리턴해준다! (=> 이전 시간과 지금 시간을 알 수 있다.) (1000ms가 1초)
	// g_PrevTime 변수 <DWORD>
	// 현재 시간은 api로 받아온다 -> currTime변수
	// elapsedTime = currTime - g_PrevTime (저 변수를 1000으로 나누어서 초 단위로 변경한다.)
	
	Age += eTime;

	if (Age > MaxAge)
		HP = 0.f;

	CurrCoolTime += eTime;

	// calc friction(마찰력)
	// 정규화 과정 (벡터 방향을 그대로 두지만 크기가 1인 단위벡터로 바꾸는 과정)
	float magVel = sqrtf(Vel_x * Vel_x + Vel_y * Vel_y + Vel_z * Vel_z);
	float velX = Vel_x / magVel;
	float velY = Vel_y / magVel;
	float velZ = Vel_z / magVel;

	// 마찰력이 작용하는 방향도 구함(힘의 반대방향이므로)
	float fricX = -velX;
	float fricY = -velY;

	// 마찰력의 크기
	float friction = CoefFrict * Mass * 9.8f; // 마찰력 = 마찰계수 * m(질량) * g(중력)

	// 마찰력 방향 * 마찰력 크기
	fricX *= friction;
	fricY *= friction;

	if (magVel < FLT_EPSILON)		// float변수는 == 연산 하지말고 범위 연산으로 해야한다! (0보다 작은 경우)
	{
		Vel_x = 0.f;
		Vel_y = 0.f;
		Vel_z = 0.f;
	}
	else
	{
		float accX = fricX / Mass;
		float accY = fricY / Mass;

		float afterVelX = Vel_x + accX * eTime;
		float afterVelY = Vel_y + accY * eTime;

		if (afterVelX * Vel_x < 0.f)
			Vel_x = 0.f;
		else
			Vel_x = afterVelX;

		if (afterVelY * Vel_y < 0.f)
			Vel_y = 0.f;
		else
			Vel_y = afterVelY;

		// Gravity
		Vel_z = Vel_z - GRAVITY * eTime;
	}

	//calc velocity
	Vel_x = Vel_x + Accel_x * eTime;
	Vel_y = Vel_y + Accel_y * eTime;
	Vel_z = Vel_z + Accel_z * 0.1f;

	// calc pos
	float tmpPos_x, tmpPos_y;
	tmpPos_x = Pos_x + Vel_x * eTime;
	tmpPos_y = Pos_y + Vel_y * eTime;
	Pos_z = Pos_z + Vel_z * eTime;

	int kind;
	GetKind(&kind);

	if (kind == KIND_HERO_LEFT || kind == KIND_HERO_RIGHT)
	{
		if (tmpPos_x > 3.3f)
		{
			tmpPos_x = 3.3f;
		}
		if (tmpPos_x < -3.3f)
		{
			tmpPos_x = -3.3f;
		}
		if (tmpPos_y > 2.0f)
		{
			tmpPos_y = 2.0f;
		}
		if (tmpPos_y < -2.4f)
		{
			tmpPos_y = -2.4f;
		}

		if (Pos_x < tmpPos_x)	// D Pressed
		{
			SetKind(KIND_HERO_RIGHT);
		}
		if (Pos_x > tmpPos_x)	// A Pressed
		{
			SetKind(KIND_HERO_LEFT);
		}
	}

	Pos_x = tmpPos_x;
	Pos_y = tmpPos_y;


	if (Pos_z > 0.f)
	{
		State = STATE_AIR;
	}
	else
	{
		State = STATE_GROUND;
		velZ = 0.f;
		Pos_z = 0.f;
	}
}

void Object::GetPos(float * x, float * y, float* z)
{
	*x = Pos_x;
	*y = Pos_y;
	*z = Pos_z;
}

void Object::SetPos(float x, float y, float z)
{
	Pos_x = x;
	Pos_y = y;
	Pos_z = z;
}

void Object::GetVel(float* x, float* y, float* z )
{
	*x = Vel_x;
	*y = Vel_y;
	*z = Vel_z;
}


void Object::SetVel(float x, float y, float z)
{
	Vel_x = x;
	Vel_y = y;
	Vel_z = z;
}

void Object::GetAccel(float * x, float * y, float* z)
{
	*x = Accel_x;
	*y = Accel_y;
	*z = Accel_z;
}

void Object::SetAccel(float x, float y, float z)
{
	Accel_x = x;
	Accel_y = y;
	Accel_z = z;
}

void Object::GetSize(float * x, float * y, float* z)
{
	*x = Size_x;
	*y = Size_y;
	*z = Size_z;
}

void Object::SetSize(float x, float y, float z)
{
	Size_x = x;
	Size_y = y;
	Size_z = z;
}

void Object::GetMass(float * x)
{
	*x = Mass;
}

void Object::SetMass(float x)
{
	Mass = x;
}

void Object::GetColor(float * r, float * g, float * b, float* a)
{
	*r = Color_r;
	*g = Color_g;
	*b = Color_b;
	*a = Color_a;
}

void Object::SetColor(float r, float g, float b, float a)
{
	Color_r = r;
	Color_g = g;
	Color_b = b;
	Color_a = a;
}

void Object::GetCoefFrict(float* f)
{
	*f = CoefFrict;
}

void Object::SetCoefFrict(float f)
{
	CoefFrict = f;
}

void Object::ApplyForce(float x, float y, float z, float eTime)
{
	// calc accel
	Accel_x = x / Mass;
	Accel_y = y / Mass;
	Accel_z = z / Mass;

	// calc vel
	Vel_x = Vel_x + Accel_x * eTime;
	Vel_y = Vel_y + Accel_y * eTime;
	Vel_z = Vel_z + Accel_z * eTime;

	// 0.f으로 set 해주지않으면 계속 빨라진다!
	Accel_x = 0.f;
	Accel_y = 0.f;
	Accel_z = 0.f;
}


void Object::GetKind(int * kind)
{
	*kind = Kind;
}

void Object::SetKind(int kind)
{
	Kind = kind;
}

void Object::GetHP(int * hp)
{
	*hp = HP;
}

void Object::SetHP(int hp)
{
	HP = hp;
}

void Object::GetState(int * state)
{
	 *state = State;
}

void Object::SetState(int state)
{
	State = state;
}

void Object::GetAge(float* age)
{
	*age = Age;
}

void Object::SetAge(float age)
{
	Age = age;
}

void Object::SetMaxAge(float maxAge)
{
	MaxAge = maxAge;
}

void Object::ResetCoolTime()
{
	CurrCoolTime = 0.f;
}

bool Object::CanFire()
{
	if (CurrCoolTime >= CoolTime)
	{
		return true;
	}
	else
		return false;
}

bool Object::CanGetHurt()
{
	if (CurrCoolTime >= 0.5f)
	{
		return true;
	}
	else
		return false;
}

void Object::SetCooltime(float coolTime)
{
	CoolTime = coolTime;
}
