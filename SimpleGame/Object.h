#pragma once

// position이 velocity(속도), acceleration(가속도)를 가지고
// 위치를 옮겨주는 update 함수를 만든다.

class Object
{
public:
	Object();
	~Object();

	void Update(float eTime);

	void GetPos(float* x, float* y, float* z);
	void SetPos(float x, float y, float z);
	void GetVel(float* x, float* y, float* z);
	void SetVel(float x, float y, float z);
	void GetAccel(float* x, float* y, float* z);
	void SetAccel(float x, float y, float z);
	void GetSize(float* x, float* y, float* z);
	void SetSize(float x, float ym, float z);
	void GetMass(float* x);
	void SetMass(float x);
	void GetColor(float* r, float* g, float* b, float* a);
	void SetColor(float r, float g, float b, float a);
	void GetCoefFrict(float* f);
	void SetCoefFrict(float f);
	void ApplyForce(float x, float y, float z, float eTime);
	void GetKind(int* kind);
	void SetKind(int kind);
	void GetHP(int* hp);
	void SetHP(int hp);
	void GetState(int* state);
	void SetState(int state);
	void GetAge(float* age);
	void SetAge(float age);

	void SetMaxAge(float maxAge);

	void ResetCoolTime();
	bool CanFire();

	bool CanGetHurt();

	void SetCooltime(float coolTime);

private:
	float Pos_x, Pos_y, Pos_z;
	float Vel_x, Vel_y, Vel_z;
	float Accel_x, Accel_y, Accel_z;

	float Mass;
	float Size_x, Size_y, Size_z;
	float Color_r, Color_g, Color_b, Color_a;

	float CoefFrict;	// 마찰계수

	int HP;
	int Kind;
	int State;

	float Charistic;	// 특성(characteristic)
	
	float Age;
	float MaxAge;

	float CoolTime;		// 몇초마다 쏠 것인기		// get, set 필요하면 만들기
	float CurrCoolTime;	// 지난번 쏜 이후로 몇초 지났는가
};

