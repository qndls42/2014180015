#include "stdafx.h"
#include "ScnMgr.h"
#include "Global.h"

ScnMgr::ScnMgr()
{
	m_Renderer = NULL;

	CurrScene = TITLE_SCENE;

	LeftBuildings = NUM_BUILDING;

	// Initialize renderer
	m_Renderer = new Renderer(700, 500);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	// Load Texture
	m_TextureBuildingA = m_Renderer->CreatePngTexture("./Textures/Building_A.png");
	m_TextureBuildingB = m_Renderer->CreatePngTexture("./Textures/Building_B.png");
	m_TextureBuildingC = m_Renderer->CreatePngTexture("./Textures/Building_C.png");
	m_TextureBuildingD = m_Renderer->CreatePngTexture("./Textures/Building_D.png");

	m_TextureTitle = m_Renderer->CreatePngTexture("./Textures/Title.png");
	m_TextureEnd = m_Renderer->CreatePngTexture("./Textures/End.png");
	m_TextureMainRight = m_Renderer->CreatePngTexture("./Textures/MainCharacter_Right.png");
	m_TextureMainLeft = m_Renderer->CreatePngTexture("./Textures/MainCharacter_Left.png");
	m_TextureLaser = m_Renderer->CreatePngTexture("./Textures/Laser.png");
	m_TextureBG = m_Renderer->CreatePngTexture("./Textures/Background.png");
	m_TextureEffSeq = m_Renderer->CreatePngTexture("./Textures/Explosion.png");

	m_Sound = new Sound();
	m_SoundBG = m_Sound->CreateSound("./Sounds/BG.mp3");
	m_SoundFire = m_Sound->CreateSound("./Sounds/Fire.mp3");
	m_SoundExplosion = m_Sound->CreateSound("./Sounds/Explosion.mp3");
	m_SoundWin = m_Sound->CreateSound("./Sounds/Win.mp3");
	m_SoundLose = m_Sound->CreateSound("./Sounds/Lose.mp3");
	m_SoundHurt = m_Sound->CreateSound("./Sounds/Hurt.mp3");

	m_Sound->PlaySound(m_SoundBG, true, 2.f);

	// Init Objects List
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		m_Objects[i] = NULL;
	}
	// Create Hero Object
	m_Objects[HERO_ID] = new Object();
	m_Objects[HERO_ID]->SetPos(0.0f, 0.0f, 0.0f);
	m_Objects[HERO_ID]->SetVel(0.0f, 0.0f, 0.f);
	m_Objects[HERO_ID]->SetAccel(0.0f, 0.0f, 0.f);
	m_Objects[HERO_ID]->SetSize(0.5f, 0.5f, 0.5f);
	m_Objects[HERO_ID]->SetMass(1.0f);
	m_Objects[HERO_ID]->SetColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Objects[HERO_ID]->SetCoefFrict(COEF_HERO);
	m_Objects[HERO_ID]->SetKind(KIND_HERO_RIGHT);
	m_Objects[HERO_ID]->SetHP(HP_HERO);
	m_Objects[HERO_ID]->SetState(STATE_GROUND);

	// Create Building_A
	AddObject(2.8f, 1.7f, 0.f,
		1.1f, 1.0f, 1.f,
		0.f, 0.f, 0.f,
		KIND_BUILDING_A,
		HP_BUILDING,
		STATE_GROUND, FLT_MAX);

	// Create Building_B
	AddObject(-2.8f, 1.7f, 0.f,
		1.1f, 1.0f, 1.f,
		0.f, 0.f, 0.f,
		KIND_BUILDING_B,
		HP_BUILDING,
		STATE_GROUND, FLT_MAX);

	// Create Building_C
	AddObject(2.8f, -1.9f, 0.f,
		1.1f, 1.0f, 1.f,
		0.f, 0.f, 0.f,
		KIND_BUILDING_C,
		HP_BUILDING,
		STATE_GROUND, FLT_MAX);

	// Create Building_D
	AddObject(-2.8f, -1.9f, 0.f,
		1.1f, 1.0f, 1.f,
		0.f, 0.f, 0.f,
		KIND_BUILDING_D,
		HP_BUILDING,
		STATE_GROUND, FLT_MAX);
}

ScnMgr::~ScnMgr()
{
	if (m_Renderer) {
		delete m_Renderer;
		m_Renderer = NULL;
	}

	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i])
		{
			delete m_Objects[i];
			m_Objects[i] = NULL;
		}
	}
}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.2f, 0.35f, 1.0f);

	switch (CurrScene)
	{
	case TITLE_SCENE:
		m_Renderer->DrawTextureRectDepth(0.f, 0.f, 0.f, 700.f, 500.f, 1.f, 1.f, 1.f, 1.f, m_TextureTitle, 1.f);
		break;
	case GAMEPLAY_SCENE:
		m_Renderer->DrawTextureRectDepth(0.f, 0.f, 0.f, 700.f, 500.f, 1.f, 1.f, 1.f, 1.f, m_TextureBG, 1.f);

		for (int i = 0; i < MAX_OBJECTS; i++)
		{
			int kind = -1;
			int hp;

			if (m_Objects[i] != NULL)
			{
				m_Objects[i]->GetKind(&kind);

				float x, y, z;
				m_Objects[i]->GetPos(&x, &y, &z);
				float sizeX, sizeY, sizeZ;
				m_Objects[i]->GetSize(&sizeX, &sizeY, &sizeZ);
				float r, g, b, a = 1.f;
				m_Objects[i]->GetColor(&r, &g, &b, &a);

				float newX, newY, newZ, newW, newH;
				newX = x * 100.f;
				newY = y * 100.f;
				newZ = z * 100.f;
				newW = sizeX * 100.f;
				newH = sizeY * 100.f;

				if (kind == KIND_HERO_RIGHT || kind == KIND_HERO_LEFT
					|| kind == KIND_BUILDING_A || kind == KIND_BUILDING_B || kind == KIND_BUILDING_C || kind == KIND_BUILDING_D)
				{
					float hpPer = 1.f;

					m_Objects[i]->GetHP(&hp);

					if (kind == KIND_HERO_RIGHT || kind == KIND_HERO_LEFT)
					{
						hpPer = (float)hp / HP_HERO;
					}
					else if (kind == KIND_BUILDING_A || kind == KIND_BUILDING_B || kind == KIND_BUILDING_C || kind == KIND_BUILDING_D)
					{
						hpPer = (float)hp / HP_BUILDING;
					}

					float age = 0.f;
					float seqNum = 0.f;
					int seqNumInt = 0;

					switch (kind)
					{
					case KIND_HERO_LEFT:
					case KIND_HERO_RIGHT:
						// 각 kind에 맞게 hp값을 이용해서 0.5f 값을 변수로 만들면 된다.
						m_Renderer->DrawSolidRectGauge(
							newX, newY + newH, 0,
							newW, 3.f,
							r, 0.f, 0.f, a,
							newZ,
							hpPer);

						m_Objects[i]->GetAge(&age);
						seqNum = age / 1.f;
						seqNum *= 10.f;
						seqNumInt = ((int)seqNum) % 10;

						if (kind == KIND_HERO_RIGHT)
						{
							m_Renderer->DrawTextureRectSeqXYHeight(
								newX, newY, 0,
								newW, newH,
								r, g, b, a,
								m_TextureMainRight,
								seqNumInt, 0, 10, 1, newZ
							);
						}
						else
						{
							m_Renderer->DrawTextureRectSeqXYHeight(
								newX, newY, 0,
								newW, newH,
								r, g, b, a,
								m_TextureMainLeft,
								seqNumInt, 0, 10, 1, newZ
							);
						}

						break;
					case KIND_BUILDING_A:
					case KIND_BUILDING_B:
					case KIND_BUILDING_C:
					case KIND_BUILDING_D:
						m_Renderer->DrawSolidRectGauge(
							newX, newY + newH / 1.7f, 0,
							newW, 3.f,
							r, 0.f, 0.f, a,
							newZ,
							hpPer);

						switch (kind)
						{
						case KIND_BUILDING_A:
							m_Renderer->DrawTextureRect(
								newX, newY, newZ,
								newW, newH,
								r, g, b, a,
								m_TextureBuildingA
							);
							break;
						case KIND_BUILDING_B:
							m_Renderer->DrawTextureRect(
								newX, newY, newZ,
								newW, newH,
								r, g, b, a,
								m_TextureBuildingB
							);
							break;
						case KIND_BUILDING_C:
							m_Renderer->DrawTextureRect(
								newX, newY, newZ,
								newW, newH,
								r, g, b, a,
								m_TextureBuildingC
							);
							break;
						case KIND_BUILDING_D:
							m_Renderer->DrawTextureRect(
								newX, newY, newZ,
								newW, newH,
								r, g, b, a,
								m_TextureBuildingD
							);
							break;
						default:
							break;
						}
						break;
					default:
						break;
					}
				}

				if (kind == KIND_BULLET_HERO)
				{
					m_Renderer->DrawTextureRectHeight(
						newX, newY, newZ,
						6.f, 6.f,
						r, g, b, a,
						m_TextureLaser,
						newZ);
				}

				if (kind == KIND_BULLET_BUILDING)
				{
					m_Renderer->DrawTextureRectHeight(
						newX, newY, newZ,
						10.f, 10.f,
						1.f, 0.f, 0.f, a,
						m_TextureLaser,
						newZ);
				}

				if (kind == KIND_EFFECT)
				{
					float age;
					m_Objects[i]->GetAge(&age);
					float seqNum = age / 0.5f;
					seqNum *= 10.f;
					int seqNumInt = ((int)seqNum) % 10;

					m_Renderer->DrawTextureRectSeqXYHeight(
						newX, newY, 0,
						newW, newH,
						r, g, b, a,
						m_TextureEffSeq,
						seqNumInt, 0, 10, 1, newZ
					);
				}
			}
		}
		break;
	case END_SCENE:
		m_Renderer->DrawTextureRectDepth(0.f, 0.f, 0.f, 700.f, 500.f, 1.f, 1.f, 1.f, 1.f, m_TextureEnd, 1.f);
		break;
	default:
		break;
	}
}

// 게임은 컴퓨터에 상관없이 동일하게 플레이가 되어야하기 때문에
// 시간을 체크할 때 꼭 조심!

// 시간 단위: 초(sec)
// 
// 속도 = 이전 속도 + (시간 * 가속도)
// 위치 = 이전 위치 + (시간 * 속도)
// 시간은 1/60초 가정.

// 100 픽셀 = 1 미터로 가정!
void ScnMgr::Update(float eTime)
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
			m_Objects[i]->Update(eTime);
	}
}

void ScnMgr::DoCollisionTest()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
		{
			continue;
		}

		int collisionCount = 0;

		for (int j = i + 1; j < MAX_OBJECTS; j++)
		{
			if (m_Objects[j] == NULL)
			{
				continue;
			}

			if (i == j)
			{
				continue;
			}

			// i object 정보
			float pX, pY, pZ;
			float sizeX, sizeY, sizeZ;
			float minX, minY, minZ, maxX, maxY, maxZ;
			m_Objects[i]->GetPos(&pX, &pY, &pZ);
			m_Objects[i]->GetSize(&sizeX, &sizeY, &sizeZ);
			minX = pX - sizeX / 2.f;
			maxX = pX + sizeX / 2.f;
			minY = pY - sizeY / 2.f;
			maxY = pY + sizeY / 2.f;
			minZ = pZ - sizeZ / 2.f;
			maxZ = pZ + sizeZ / 2.f;

			// j object 정보
			float pX1, pY1, pZ1;
			float sizeX1, sizeY1, sizeZ1;
			float minX1, minY1, minZ1, maxX1, maxY1, maxZ1;
			m_Objects[j]->GetPos(&pX1, &pY1, &pZ1);
			m_Objects[j]->GetSize(&sizeX1, &sizeY1, &sizeZ1);
			minX1 = pX1 - sizeX1 / 2.f;
			maxX1 = pX1 + sizeX1 / 2.f;
			minY1 = pY1 - sizeY1 / 2.f;
			maxY1 = pY1 + sizeY1 / 2.f;
			minZ1 = pZ1 - sizeZ1 / 2.f;
			maxZ1 = pZ1 + sizeZ1 / 2.f;

			if (BBCollision(
				minX, minY, minZ,
				maxX, maxY, maxZ,
				minX1, minY1, minZ1,
				maxX1, maxY1, maxZ1))
			{
				int iKind;
				int jKind;
				m_Objects[i]->GetKind(&iKind);
				m_Objects[j]->GetKind(&jKind);

				if ( (iKind == KIND_BULLET_HERO && jKind == KIND_BULLET_HERO) ||
					 (iKind == KIND_BULLET_BUILDING && jKind == KIND_BULLET_BUILDING) )// 총알끼리 충돌 예외처리
				{
					continue;
				}
				else if ((iKind == KIND_BULLET_HERO && jKind == KIND_EFFECT) ||
						 (iKind == KIND_BULLET_BUILDING && jKind == KIND_EFFECT)) // 총알과 이펙트 충돌 예외처리
				{
					continue;
				}
				else if (i == HERO_ID && jKind == KIND_BULLET_HERO)	// 총알과 히어로의 충돌 예외처리
				{
					continue;
				}
				else if ((iKind == KIND_BUILDING_A || iKind == KIND_BUILDING_B || iKind == KIND_BUILDING_C || iKind == KIND_BUILDING_D)
					&& jKind == KIND_BULLET_BUILDING)
				{
					continue;
				}
				else
				{
					//std::cout << "Collision\n"; // ★
					collisionCount++;
					ProcessCollision(i, j);
				}
			}
		}
		if (collisionCount > 0)
		{
			m_Objects[i]->SetColor(1.f, 0.f, 0.f, 1.f);
		}
		else
		{
			m_Objects[i]->SetColor(1.f, 1.f, 1.f, 1.f);

		}
	}
}

void ScnMgr::GarbageCollector()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			int kind;
			m_Objects[i]->GetKind(&kind);

			int hp = -1;
			m_Objects[i]->GetHP(&hp);

			float x, y, z;
			m_Objects[i]->GetPos(&x, &y, &z);

			float vx, vy, vz, mag;
			m_Objects[i]->GetVel(&vx, &vy, &vz);
			mag = sqrt(vx*vx + vy * vy + vz * vz);


			if (kind == KIND_BULLET_HERO || kind == KIND_BULLET_BUILDING || kind == KIND_EFFECT)
			{
				if (hp <= 0)
				{
					DeleteObject(i);
					continue;
				}
			}
			if (kind == KIND_BUILDING_A || kind == KIND_BUILDING_B || kind == KIND_BUILDING_C || kind == KIND_BUILDING_D)
			{
				if (hp <= 0)
				{
					DeleteObject(i);
					LeftBuildings--;
					std::cout << "Left Buildings: " << LeftBuildings << std::endl;
					if (LeftBuildings <= 0)
					{
						//m_Sound->DeleteSound(m_SoundBG);
						m_Sound->PlaySound(m_SoundWin, false, 3.f);
						SetScene(END_SCENE);
					}

					continue;
				}
			}
			if (kind == KIND_HERO_LEFT || kind == KIND_HERO_RIGHT)
			{
				if (hp <= 0)
				{
					DeleteObject(i);

					m_Sound->PlaySound(m_SoundLose, false, 3.f);
					SetScene(END_SCENE);
					continue;
				}
			}
			if (kind == KIND_BULLET_HERO || kind == KIND_BULLET_BUILDING)
			{
				if (x > 3.5f || x < -3.5f || y > 2.5f || y < -2.5f)
				{
					DeleteObject(i);
					continue;
				}

				if (mag < FLT_EPSILON)	// float에서의 오차가 날 수 있는 가장 작은 값보다 작아진다면(즉, 이것은 0이라고 볼 수 있다.)
				{
					DeleteObject(i);
					continue;
				}
			}
		}
	}
}

bool ScnMgr::RRCollision(float minX, float minY, float maxX, float maxY,
	float minX1, float minY1, float maxX1, float maxY1)
{
	if (minX < minX1)
		return false;
	if (maxX1 < minX)
		return false;
	if (maxY < minY1)
		return false;
	if (maxY1 < minY)
		return false;
	return true;
}

bool ScnMgr::BBCollision(float minX, float minY, float minZ, float maxX, float maxY, float maxZ,
	float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1)
{
	if (maxX < minX1)
		return false;
	if (maxX1 < minX)
		return false;
	if (maxY < minY1)
		return false;
	if (maxY1 < minY)
		return false;
	if (maxZ < minZ1)
		return false;
	if (maxZ1 < minZ)
		return false;

	return true;
}

void ScnMgr::ProcessCollision(int i, int j)
{
	if (m_Objects[i] == NULL || m_Objects[j] == NULL)
		return;

	Object *obj1 = m_Objects[i];
	Object *obj2 = m_Objects[j];

	int kind1, kind2;
	obj1->GetKind(&kind1);
	obj2->GetKind(&kind2);

	if ((kind1 == KIND_BUILDING_A || kind1 == KIND_BUILDING_B || kind1 == KIND_BUILDING_C || kind1 == KIND_BUILDING_D) 
		&& kind2 == KIND_BULLET_HERO)
	{
		int hp1, hp2;

		obj1->GetHP(&hp1);	// Building
		obj2->GetHP(&hp2);	// Bullet

		hp1 = hp1 - hp2;	// apply damage
		hp2 = 0;

		obj1->SetHP(hp1);
		obj2->SetHP(hp2);

		if (hp1 <= 0)
			m_Sound->PlaySound(m_SoundExplosion, false, 3.f);

		float x, y, z;
		obj2->GetPos(&x, &y, &z);

		AddObject(x, y, z, 1.f, 1.f, 1.f, 0, 0, 0,		// ★ 속도는 오브젝트 속도에 맞춰서!
			KIND_EFFECT, 10, STATE_AIR, 0.5f);
	}

	if (kind1 == KIND_BULLET_HERO && 
		(kind2 == KIND_BUILDING_A || kind2 == KIND_BUILDING_B || kind2 == KIND_BUILDING_C || kind2 == KIND_BUILDING_D))
	{
		int hp1, hp2;

		obj1->GetHP(&hp1);	// Bullet
		obj2->GetHP(&hp2);	// Building

		hp2 = hp2 - hp1;	// apply damage
		hp1 = 0;

		obj1->SetHP(hp1);
		obj2->SetHP(hp2);

		if (hp2 <= 0)
			m_Sound->PlaySound(m_SoundExplosion, false, 3.f);

		float x, y, z;
		obj1->GetPos(&x, &y, &z);

		AddObject(x, y, z, 1.f, 1.f, 1.f, 0, 0, 0,		// ★ 속도는 오브젝트 속도에 맞춰서!
			KIND_EFFECT, 10, STATE_AIR, 0.5f);
	}

	if ((kind1 == KIND_HERO_LEFT || kind1 == KIND_HERO_RIGHT) && kind2 == KIND_BULLET_BUILDING)
	{
		int hp1, hp2;

		obj1->GetHP(&hp1);	// Hero
		obj2->GetHP(&hp2);	// Bullet

		hp1 = hp1 - (hp2 * 10);	// apply damage
		hp2 = 0;

		obj1->SetHP(hp1);
		obj2->SetHP(hp2);

		m_Sound->PlaySound(m_SoundHurt, false, 2.f);

		float x, y, z;
		obj2->GetPos(&x, &y, &z);

		AddObject(x, y, z, 0.5f, 0.5f, 0.5f, 0, 0, 0,		// ★ 속도는 오브젝트 속도에 맞춰서!
			KIND_EFFECT, 10, STATE_AIR, 0.5f);
	}

	if ((kind2 == KIND_HERO_LEFT || kind2 == KIND_HERO_RIGHT) && kind1 == KIND_BULLET_BUILDING)
	{
		int hp1, hp2;

		obj2->GetHP(&hp2);	// Hero
		obj1->GetHP(&hp1);	// Bullet

		hp2 = hp2 - (hp1 * 10);	// apply damage
		hp1 = 0;

		obj2->SetHP(hp2);
		obj1->SetHP(hp1);

		m_Sound->PlaySound(m_SoundHurt, false, 2.f);

		float x, y, z;
		obj2->GetPos(&x, &y, &z);

		AddObject(x, y, z, 0.5f, 0.5f, 0.5f, 0, 0, 0,		// ★ 속도는 오브젝트 속도에 맞춰서!
			KIND_EFFECT, 10, STATE_AIR, 0.5f);
	}


	if (m_Objects[HERO_ID] != NULL && !m_Objects[HERO_ID]->CanGetHurt())
	{
		return;
	}

	if ((kind1 == KIND_BUILDING_A || kind1 == KIND_BUILDING_B || kind1 == KIND_BUILDING_C || kind1 == KIND_BUILDING_D) &&
		(kind2 == KIND_HERO_RIGHT || kind2 == KIND_HERO_LEFT))
	{
		int hp1, hp2;

		obj1->GetHP(&hp1);	// Building
		obj2->GetHP(&hp2);	// Hero

		hp1 = hp1 - HP_BULLET;	// apply damage
		hp2 = hp2 - HP_BUILDING;

		obj1->SetHP(hp1);
		obj2->SetHP(hp2);

		if (hp1 <= 0)
			m_Sound->PlaySound(m_SoundExplosion, false, 3.f);

		m_Sound->PlaySound(m_SoundHurt, false, 2.f);

		float x, y, z;
		obj2->GetPos(&x, &y, &z);

		AddObject(x, y, z, 1.f, 1.f, 1.f, 0, 0, 0,		// ★ 속도는 오브젝트 속도에 맞춰서!
			KIND_EFFECT, 10, STATE_AIR, 0.5f);
	}

	if ((kind1 == KIND_HERO_RIGHT || kind1 == KIND_HERO_LEFT) &&
		(kind2 == KIND_BUILDING_A || kind2 == KIND_BUILDING_B || kind2 == KIND_BUILDING_C || kind2 == KIND_BUILDING_D))
	{
		int hp1, hp2;

		obj1->GetHP(&hp1);	// Hero
		obj2->GetHP(&hp2);	// Building

		hp2 = hp2 - HP_BULLET;	// apply damage
		hp1 = hp1 - HP_BUILDING;

		obj1->SetHP(hp1);
		obj2->SetHP(hp2);

		if (hp2 <= 0)
			m_Sound->PlaySound(m_SoundExplosion, false, 3.f);

		m_Sound->PlaySound(m_SoundHurt, false, 2.f);

		float x, y, z;
		obj1->GetPos(&x, &y, &z);

		AddObject(x, y, z, 1.f, 1.f, 1.f, 0, 0, 0,		// ★ 속도는 오브젝트 속도에 맞춰서!
			KIND_EFFECT, 10, STATE_AIR, 0.5f);
	}

	if (m_Objects[HERO_ID] != NULL)
		m_Objects[HERO_ID]->ResetCoolTime();
}

void ScnMgr::GetScene(int * scene)
{
	*scene = CurrScene;
}

void ScnMgr::SetScene(int scene)
{
	CurrScene = scene;
}

void ScnMgr::ApplyForce(float x, float y, float z, float eTime)
{
	if (m_Objects[HERO_ID] != NULL)
	{
		int state;
		m_Objects[HERO_ID]->GetState(&state);
		if (state == STATE_AIR)
		{
			z = 0.f;
		}
		m_Objects[HERO_ID]->ApplyForce(x, y, z, eTime);
	}
}

void ScnMgr::Shoot(int shootID)
{
	if (m_Objects[HERO_ID] != NULL && CurrScene == GAMEPLAY_SCENE)
	{
		// 건물 발사
		for (int i = 1; i < NUM_BUILDING+1; i++)
		{
			float hero_x, hero_y, hero_z;
			float pX, pY, pZ;
			float bvX, bvY, bvZ;
			float magVel = 0.f;
			int kind = -1;
			if (m_Objects[i] != NULL)
			{
				m_Objects[i]->GetKind(&kind);
			}
			if (m_Objects[i] != NULL &&
				(kind == KIND_BUILDING_A || kind == KIND_BUILDING_B || kind == KIND_BUILDING_C || kind == KIND_BUILDING_D))
			{
				switch (kind)
				{
				case KIND_BUILDING_A:
					m_Objects[i]->SetCooltime(2.0f);

					if (!m_Objects[i]->CanFire())
					{
						break;
					}

					m_Objects[HERO_ID]->GetPos(&hero_x, &hero_y, &hero_z);
					m_Objects[i]->GetPos(&pX, &pY, &pZ);

					magVel = sqrtf((pX - hero_x) * (pX - hero_x) + (pY - hero_y) * (pY - hero_y) + (pZ - hero_z) * (pZ - hero_z));

					bvX = 0.f;
					bvY = 0.f;
					bvZ = 0.f;

					bvX = ((hero_x - pX) / magVel) * 2.0f;
					bvY = ((hero_y - pY) / magVel) * 2.0f;
					bvZ = ((hero_z - pZ) / magVel);

					AddObject(
						pX, pY, pZ,
						0.1f, 0.1f, 0.1f,
						bvX, bvY, bvZ,
						KIND_BULLET_BUILDING,
						HP_BULLET * 2,
						STATE_AIR, FLT_MAX);

					m_Objects[i]->ResetCoolTime();
					break;
				case KIND_BUILDING_B:
					m_Objects[i]->SetCooltime(0.7f);

					if (!m_Objects[i]->CanFire())
					{
						break;
					}

					m_Objects[HERO_ID]->GetPos(&hero_x, &hero_y, &hero_z);
					m_Objects[i]->GetPos(&pX, &pY, &pZ);

					bvX = 1.f;
					bvY = 0.f;
					bvZ = 0.f;

					AddObject(
						pX, pY, pZ,
						0.1f, 0.1f, 0.1f,
						bvX, bvY, bvZ,
						KIND_BULLET_BUILDING,
						HP_BULLET * 2,
						STATE_AIR, FLT_MAX);

					bvX = 0.f;
					bvY = -1.f;

					AddObject(
						pX, pY, pZ,
						0.1f, 0.1f, 0.1f,
						bvX, bvY, bvZ,
						KIND_BULLET_BUILDING,
						HP_BULLET * 2,
						STATE_AIR, FLT_MAX);

					m_Objects[i]->ResetCoolTime();
					break;
				case KIND_BUILDING_C:
					m_Objects[i]->SetCooltime(5.0f);

					if (!m_Objects[i]->CanFire())
					{
						break;
					}

					m_Objects[HERO_ID]->GetPos(&hero_x, &hero_y, &hero_z);
					m_Objects[i]->GetPos(&pX, &pY, &pZ);

					magVel = sqrtf((pX - hero_x) * (pX - hero_x) + (pY - hero_y) * (pY - hero_y) + (pZ - hero_z) * (pZ - hero_z));

					bvX = 0.f;
					bvY = 0.f;
					bvZ = 0.f;

					bvX = ((hero_x - pX) / magVel) * 0.7f;
					bvY = ((hero_y - pY) / magVel) * 0.7f;
					bvZ = ((hero_z - pZ) / magVel);

					AddObject(
						pX, pY, pZ,
						0.1f, 0.1f, 0.1f,
						bvX, bvY, bvZ,
						KIND_BULLET_BUILDING,
						HP_BULLET * 20,
						STATE_AIR, FLT_MAX);

					m_Objects[i]->ResetCoolTime();
					break;
				case KIND_BUILDING_D:
					m_Objects[i]->SetCooltime(3.0f);

					if (!m_Objects[i]->CanFire())
					{
						break;
					}

					m_Objects[HERO_ID]->GetPos(&hero_x, &hero_y, &hero_z);
					m_Objects[i]->GetPos(&pX, &pY, &pZ);

					magVel = sqrtf((pX - hero_x) * (pX - hero_x) + (pY - hero_y) * (pY - hero_y) + (pZ - hero_z) * (pZ - hero_z));

					bvX = 0.f;
					bvY = 0.f;
					bvZ = 0.f;

					bvX = ((hero_x - pX) / magVel) * 1.0f;
					bvY = ((hero_y - pY) / magVel) * 1.0f;
					bvZ = ((hero_z - pZ) / magVel) + 5.f;

					AddObject(
						pX, pY, pZ,
						0.1f, 0.1f, 0.1f,
						bvX, bvY, bvZ,
						KIND_BULLET_BUILDING,
						HP_BULLET * 2,
						STATE_AIR, FLT_MAX);

					m_Objects[i]->ResetCoolTime();
					break;
				default:
					break;
				}
			}
		}


		// 히어로 발사
		if (shootID == SHOOT_NONE)
		{
			return;
		}

		if (!m_Objects[HERO_ID]->CanFire()) {
			return;
		}

		float amount = 3.f;
		float bvX, bvY, bvZ;	// 총알 속도

		bvX = 0.f;
		bvY = 0.f;
		bvZ = 0.f;

		switch (shootID)
		{
		case SHOOT_UP:
			bvX = 0.f;
			bvY = amount;
			break;
		case SHOOT_DOWN:
			bvX = 0.f;
			bvY = -amount;
			break;
		case SHOOT_LEFT:
			bvX = -amount;
			bvY = 0.f;
			break;
		case SHOOT_RIGHT:
			bvX = amount;
			bvY = 0.f;
			break;
		}

		if (m_Objects[HERO_ID] != NULL)
		{
			float pX, pY, pZ;	// 플레이어 위치
			m_Objects[HERO_ID]->GetPos(&pX, &pY, &pZ);
			float vX, vY, vZ;	// 방향
			m_Objects[HERO_ID]->GetVel(&vX, &vY, &vZ);

			bvX = bvX + vX;
			bvY = bvY + vY;
			bvZ = bvZ + vZ;

			AddObject(
				pX, pY, pZ,
				0.1f, 0.1f, 0.1f,
				bvX, bvY, 0.f,
				KIND_BULLET_HERO,
				HP_BULLET,
				STATE_AIR, FLT_MAX);

			m_Sound->PlaySound(m_SoundFire, false, 3.f);
			m_Objects[HERO_ID]->ResetCoolTime();
		}
	}
}

void ScnMgr::AddObject(float x, float y, float z, float sx, float sy, float sz, float vx, float vy, float vz, int kind, int hp, int state, float maxAge)
{
	int id = FindEmptyObjectSlot();

	if (id < 0)
		return;

	m_Objects[id] = new Object();
	m_Objects[id]->SetPos(x, y, z);
	m_Objects[id]->SetVel(vx, vy, vz);
	m_Objects[id]->SetAccel(0.0f, 0.0f, 0.f);
	m_Objects[id]->SetSize(sx, sx, sz);
	m_Objects[id]->SetMass(0.4f);
	m_Objects[id]->SetColor(1.0f, 1.0f, 1.0f, 1.f);
	m_Objects[id]->SetCoefFrict(COEF_BULLET);
	m_Objects[id]->SetKind(kind);
	m_Objects[id]->SetHP(hp);
	m_Objects[id]->SetState(state);
	m_Objects[id]->SetMaxAge(maxAge);
}

int ScnMgr::FindEmptyObjectSlot()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
			return i;
	}
	std::cout << "No more empty slot" << std::endl;
	return -1;	// error check
}

void ScnMgr::DeleteObject(int id)
{
	if (m_Objects[id] != NULL)
	{
		delete m_Objects[id];
		m_Objects[id] = NULL;
	}
}