#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"
#include "Renderer.h"
#include "Object.h"
#include "Global.h"
#include "Sound.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void RenderScene();
	void Update(float eTime);
	void DoCollisionTest();

	void ApplyForce(float x, float y, float z, float eTime);
	void Shoot(int shootID);

	void AddObject(
		float x, float y, float z,
		float sx, float sy, float sz,
		float vx, float vy, float vz,
		int kind,
		int hp,
		int state,
		float maxAge);

	int FindEmptyObjectSlot();
	void DeleteObject(int id);
	void GarbageCollector();

	bool RRCollision(float minX, float minY, float maxX, float maxY,
		float minX1, float minY1, float maxX1, float maxY1);

	bool BBCollision(float minX, float minY, float minZ, float maxX, float maxY, float maxZ,
		float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1);

	void ProcessCollision(int i, int j);

	void GetScene(int* scene);
	void SetScene(int scene);

private:
	Object* m_Objects[MAX_OBJECTS];
	Renderer* m_Renderer;

	int CurrScene;

	int LeftBuildings;

	Sound *m_Sound;

	GLuint m_TextureBuildingA;
	GLuint m_TextureBuildingB;
	GLuint m_TextureBuildingC;
	GLuint m_TextureBuildingD;

	GLuint m_TextureTitle;
	GLuint m_TextureEnd;
	GLuint m_TextureMainRight;
	GLuint m_TextureMainLeft;
	GLuint m_TextureLaser;
	GLuint m_TextureBG;
	GLuint m_TextureEffSeq;

	int m_SoundBG = 0;
	int m_SoundFire = 0;
	int m_SoundExplosion = 0;
	int m_SoundWin = 0;
	int m_SoundLose = 0;
	int m_SoundHurt = 0;
};