#pragma once

#define HERO_ID 0

#define MAX_OBJECTS 1000

#define SHOOT_NONE -1
#define SHOOT_LEFT 1
#define SHOOT_RIGHT 2
#define SHOOT_UP 3
#define SHOOT_DOWN 4

#define KIND_HERO_LEFT 0
#define KIND_HERO_RIGHT 1
#define KIND_BUILDING_A 2
#define KIND_BUILDING_B 3
#define KIND_BUILDING_C 4
#define KIND_BUILDING_D 5
#define KIND_BULLET_HERO 6
#define KIND_BULLET_BUILDING 7
#define KIND_EFFECT 8

#define STATE_GROUND 0
#define STATE_AIR 1

#define GRAVITY 2.62

#define COEF_HERO 1.1
#define COEF_BULLET 0.05

#define HP_HERO 10000
#define HP_BUILDING 1000
#define HP_BULLET 20

#define TITLE_SCENE 0
#define GAMEPLAY_SCENE 1
#define END_SCENE 2

#define NUM_BUILDING 4